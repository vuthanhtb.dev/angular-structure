import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { TableComponent } from './components/table/table.component';



@NgModule({
  declarations: [
    ButtonComponent,
    TableComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ButtonComponent,
    TableComponent
  ]
})
export class SharedModule { }
